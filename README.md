# Angular Forms

### Submitted by: Sunil Prajapati
### ID: 124073

This project explores the form development methods in Angular Forms: Template Driven Method and Reactive Methods


## Project Structure

Thec components for template driven and reactive componens are at following directories:

<pre>
src
|--- app
      |--- pages
            |--- <a href="https://gitlab.com/ait_fsad-2023/team-7/angular-forms/-/tree/0.1/src/app/pages/taskone?ref_type=heads">Task One </a>
            |--- <a href="https://gitlab.com/ait_fsad-2023/team-7/angular-forms/-/tree/0.1/src/app/pages/tasktwo?ref_type=heads">Task Two </a>
            |--- <a href="https://gitlab.com/ait_fsad-2023/team-7/angular-forms/-/tree/0.1/src/app/pages/taskthree?ref_type=heads">Task Three </a>
            |--- <a href="https://gitlab.com/ait_fsad-2023/team-7/angular-forms/-/tree/0.1/src/app/pages/taskfour?ref_type=heads">Task Four </a>
                   |--- <a href="https://gitlab.com/ait_fsad-2023/team-7/angular-forms/-/tree/0.1/src/app/pages/template?ref_type=heads">template</a>
                   |      |--- <a href="https://gitlab.com/ait_fsad-2023/team-7/angular-forms/-/tree/0.1/src/app/pages/template/login?ref_type=heads">login</a>
                   |      |--- <a href="https://gitlab.com/ait_fsad-2023/team-7/angular-forms/-/tree/0.1/src/app/pages/template/register?ref_type=heads">register</a>
                   |--- <a href="https://gitlab.com/ait_fsad-2023/team-7/angular-forms/-/tree/0.1/src/app/pages/reactive?ref_type=heads">reactive </a>
                          |--- <a href="https://gitlab.com/ait_fsad-2023/team-7/angular-forms/-/tree/0.1/src/app/pages/reactive/login?ref_type=heads">login</a>

</pre>

## Prerequsites
- Node
- Angular CLI

## Running the app

First install all the packages using command:

`npm install`

To run the app, run the command:

`ng serve`

The server will start at [http:\\\localhost:4200](http:\\localhost:4200)
