import { Component } from '@angular/core';

@Component({
  selector: 'app-tasktwo',
  templateUrl: './tasktwo.component.html',
  styleUrls: ['./tasktwo.component.scss']
})
export class TasktwoComponent {
  showName: boolean = false

  countries: Array<string> = [
    'Nepal',
    'India',
    'Pakistan',
    'Bhutan',
    'Sri Lanka',
    'Bangladesh',
    'Bhutan',
    'Afghanistan'
  ]

  country: string = 'Nepal'

  toggleShowName() {
    this.showName = !this.showName
  }
}
