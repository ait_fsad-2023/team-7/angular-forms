import { Component } from '@angular/core';

@Component({
  selector: 'app-taskthree',
  templateUrl: './taskthree.component.html',
  styleUrls: ['./taskthree.component.scss']
})
export class TaskthreeComponent {
  name!: string

  onClick() {
    window.alert("You clicked the button!")
  }
}
