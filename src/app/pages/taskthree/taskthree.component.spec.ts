import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskthreeComponent } from './taskthree.component';

describe('TaskthreeComponent', () => {
  let component: TaskthreeComponent;
  let fixture: ComponentFixture<TaskthreeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TaskthreeComponent]
    });
    fixture = TestBed.createComponent(TaskthreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
