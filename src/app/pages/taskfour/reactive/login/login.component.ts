import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class ReactiveLoginComponent {
  formBuilder = new FormBuilder()

  loginForm = this.formBuilder.group({
    email: ['', Validators.required],
    password: ['', Validators.required]
  })

  emailError!: boolean
  passwordError!: boolean

  onInput () {
    this.emailError = !this.loginForm.value.email
    this.passwordError = !this.loginForm.value.password
  }

  onLogin() {
    this.onInput()

    if (this.loginForm.valid) {
      window.alert(`${this.loginForm.value.email} logged in successfully `)
    }
  }
}
