import { Component, ViewChild} from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  @ViewChild('loginForm') loginForm!: NgForm
  email!: string;
  password!: string;
  rememberMe!: boolean

  emailError: boolean = false;
  passwordError: boolean = false

  onInput () {
    this.emailError = !this.email
    this.passwordError = !this.password
  }

  onLogin() {
    if (this.loginForm.valid) {
      window.alert(`${this.email} successfully logged in`)
    } else {
      this.emailError = !this.email
      this.passwordError = !this.password
    }
  }
}
