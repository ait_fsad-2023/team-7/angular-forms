import { Component, ViewChild} from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  @ViewChild('registerForm') registerForm!: NgForm
  username!: string
  email!: string
  password!: string
  confirmPassword!: string

  userNameError: boolean = false
  emailError: boolean = false
  passwordError: boolean = false
  confirmPasswordError!: string | boolean

  onInput() {
    this.userNameError = !this.username
    this.emailError = !this.email
    this.passwordError = !this.password
    this.confirmPasswordError = this.password ? this.password == this.confirmPassword ? false : 'The password must match.' : 'The confirm password field is required.'
  }

  onRegister() {
    this.onInput()

    if (this.registerForm.valid && (this.password == this.confirmPassword)) {
      window.alert(`${this.username} registered successfully!`)
    }
  }
}
