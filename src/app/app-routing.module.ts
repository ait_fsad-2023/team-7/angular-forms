import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TaskoneComponent } from './pages/taskone/taskone.component';
import { TasktwoComponent } from './pages/tasktwo/tasktwo.component';
import { TaskthreeComponent } from './pages/taskthree/taskthree.component';
import { TaskfourComponent } from './pages/taskfour/taskfour.component';
import { LoginComponent } from './pages/taskfour/template/login/login.component';
import { RegisterComponent } from './pages/taskfour/template/register/register.component';

import { ReactiveLoginComponent } from './pages/taskfour/reactive/login/login.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "taskone",
    component: TaskoneComponent
  },
  {
    path: "tasktwo",
    component: TasktwoComponent
  },
  {
    path: 'taskthree',
    component: TaskthreeComponent
  },
  {
    path: 'taskfour',
    component: TaskfourComponent,
    children: [
      {
        path: "login",
        component: LoginComponent
      },
      {
        path: "register",
        component: RegisterComponent
      },
      {
        path: "reactive/login",
        component: ReactiveLoginComponent
      }
    ]
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
