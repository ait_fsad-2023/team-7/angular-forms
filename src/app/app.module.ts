import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { HoverDirective } from './pages/taskone/hover.directive';
import { DisableButtonDirective } from './pages/taskone/disable-button.directive';

import { LoginComponent } from './pages/taskfour/template/login/login.component';
import { RegisterComponent } from './pages/taskfour/template/register/register.component';

import { ReactiveLoginComponent } from './pages/taskfour/reactive/login/login.component';
import { TaskoneComponent } from './pages/taskone/taskone.component';
import { TasktwoComponent } from './pages/tasktwo/tasktwo.component';
import { TaskthreeComponent } from './pages/taskthree/taskthree.component';
import { TaskfourComponent } from './pages/taskfour/taskfour.component';
import { HomeComponent } from './pages/home/home.component';
@NgModule({
  declarations: [
    AppComponent,
    DisableButtonDirective,
    HoverDirective,
    LoginComponent,
    RegisterComponent,
    ReactiveLoginComponent,
    TaskoneComponent,
    TasktwoComponent,
    TaskthreeComponent,
    TaskfourComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
